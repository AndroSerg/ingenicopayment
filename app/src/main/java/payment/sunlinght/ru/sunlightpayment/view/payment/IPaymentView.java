package payment.sunlinght.ru.sunlightpayment.view.payment;

import java.util.List;

/**
 * Created by sergey on 15.08.17.
 */

public interface IPaymentView {

    void showProgress();

    void showProgress(String massege);

    void showStatusMessage(String message);

    void setDevices(List<String> devices);

    void onConnected();

    void onDisconnected();
}
