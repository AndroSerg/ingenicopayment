package payment.sunlinght.ru.sunlightpayment.view.payment;

import android.content.Context;

import java.util.List;

import payment.sunlinght.ru.sunlightpayment.data.interactor.IPaymentInteractor;

/**
 * Created by sergey on 15.08.17.
 */

public class PaymentPresenter implements IPaymentPresenter, IPaymentCallback{

    private IPaymentView mView;
    private IPaymentInteractor mInteractor;
    private String selectedDevice;
    private String mCurrentTransaction;

    public PaymentPresenter(IPaymentView view, IPaymentInteractor interactor){
        this.mView = view;
        this.mInteractor = interactor;
    }

    @Override
    public void initConnection(Context context) {
        mInteractor.setCallback(this);
        mInteractor.initConnection(context);
    }

    @Override
    public void initPayment(Context context) {
        mInteractor.initPayment(context);
    }

    @Override
    public void setSelectedDevice(String device) {
        selectedDevice = device;
    }

    @Override
    public void operation() {
        if (selectedDevice != null) mInteractor.operation(selectedDevice);
    }

    @Override
    public void paymentSuccess(String transId) {
        mCurrentTransaction = transId;
    }

    @Override
    public void devices(List<String> devices) {
        mView.setDevices(devices);
    }

    @Override
    public void deviceConnected() {
        mView.onConnected();
    }

    @Override
    public void deviceDisconnected() {
        mView.onDisconnected();
    }

    @Override
    public void startPayment(Context context) {
        mInteractor.startPayment(context, selectedDevice, this);
    }

    @Override
    public void reversalPayment() {
        mInteractor.paymentReversal(null);
    }

    @Override
    public void statusMessage(String message) {
        mView.showStatusMessage(message);
    }
}
