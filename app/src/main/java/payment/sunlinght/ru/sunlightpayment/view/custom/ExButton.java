package payment.sunlinght.ru.sunlightpayment.view.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;

import payment.sunlinght.ru.sunlightpayment.R;


public class ExButton extends AppCompatButton {
    public ExButton(Context context) {
        super(context);
    }

    public ExButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        parseAttributes(context, attrs);
    }

    public ExButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        parseAttributes(context, attrs);
    }


    private void parseAttributes(Context context, AttributeSet attrs) {
        TypedArray values = context.obtainStyledAttributes(attrs, R.styleable.ExTextView);
        int typeface = values.getInt(R.styleable.ExTextView_typeface, 0);
        setTypeface(FontsUtils.getTypeFace(context, typeface));
        values.recycle();
    }
}
