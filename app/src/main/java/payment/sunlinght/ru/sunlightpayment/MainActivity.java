package payment.sunlinght.ru.sunlightpayment;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import payment.sunlinght.ru.sunlightpayment.view.payment.PaymentFragment;

public class MainActivity extends Activity {
    private String mSelectedTag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        startFragmentByTag(PaymentFragment.TAG);
    }

    public void startFragmentByTag(String tag) {
        String oldTag = mSelectedTag;
        mSelectedTag = tag;
        final FragmentManager fm = getFragmentManager();
        final FragmentTransaction ft = fm.beginTransaction();
        final Fragment oldFragment = fm.findFragmentByTag(oldTag);
        final Fragment fragment = fm.findFragmentByTag(tag);

        if (oldFragment != null && !tag.equals(oldTag)) {
            ft.detach(oldFragment);
        }

        if (fragment == null) {
            ft.replace(R.id.container, getContentFragment(tag), tag);
        } else {
            if (fragment.isDetached()) {
                ft.attach(fragment);
            }
        }
        ft.commit();

    }

    private Fragment getContentFragment(String tag) {
        Fragment fragment = null;
        if (PaymentFragment.TAG.equals(tag)) {
            fragment = new PaymentFragment();
        }
        return fragment;
    }
}
