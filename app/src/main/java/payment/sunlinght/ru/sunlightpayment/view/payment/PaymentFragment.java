package payment.sunlinght.ru.sunlightpayment.view.payment;

import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.List;

import javax.inject.Inject;
import payment.sunlinght.ru.sunlightpayment.R;

/**
 * Created by Seleznev_S_V on 26.04.2017.
 */

public class PaymentFragment extends Fragment implements IPaymentView, View.OnClickListener, AdapterView.OnItemSelectedListener {
    public static final String TAG = "PaymentFragment";

    @Inject
    IPaymentPresenter mPaymentPresenter;

    private Spinner mDevicesList;
    private Button mPayButton;
    private Button mConnectButton;
    private Button mReverseButton;
    private TextView mTextMessage;
    private Handler handler;
    private TextView mD200;
    private TextView mICMP;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DaggerPaymentComponent.builder()
                .paymentModule(new PaymentModule(this))
                .build().inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return (inflater.inflate(R.layout.payment_fragment, container, false));
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mDevicesList = (Spinner) view.findViewById(R.id.devices_list);
        mDevicesList.setOnItemSelectedListener(this);
        mPayButton = (Button) view.findViewById(R.id.start_payment);
        mPayButton.setOnClickListener(this);
        mConnectButton = (Button) view.findViewById(R.id.connect);
        mConnectButton.setOnClickListener(this);
        mReverseButton = (Button) view.findViewById(R.id.reverse_payment);
        mReverseButton.setOnClickListener(this);
        mTextMessage = (TextView) view.findViewById(R.id.payment_status);
        mD200 = (TextView) view.findViewById(R.id.d200);
        mICMP = (TextView) view.findViewById(R.id.icmp);
        mD200.setOnClickListener(this);
        mICMP.setOnClickListener(this);
        handler = new Handler();
        mPayButton.setEnabled(false);
        mPayButton.setClickable(false);
        mConnectButton.setEnabled(false);
        mConnectButton.setClickable(false);
        mPaymentPresenter.initConnection(getActivity());
    }

    @Override
    public void showProgress() {
    }

    @Override
    public void showProgress(String massege) {
        mTextMessage.setText(massege);
    }

    @Override
    public void showStatusMessage(String message) {
        handler.post(() -> mTextMessage.setText(message));
    }

    @Override
    public void setDevices(List<String> devices) {
        handler.post(() -> {
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, devices);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mDevicesList.setAdapter(dataAdapter);
            if (devices.size() > 0) {
                mConnectButton.setEnabled(true);
                mConnectButton.setClickable(true);
            } else {
                mConnectButton.setEnabled(false);
                mConnectButton.setClickable(false);
            }
        });

    }

    @Override
    public void onConnected() {
        mPayButton.setEnabled(true);
        mPayButton.setClickable(true);
        mConnectButton.setText(getResources().getString(R.string.disconnect_button));
    }

    @Override
    public void onDisconnected() {
        mPayButton.setEnabled(false);
        mPayButton.setClickable(false);
        mConnectButton.setText(getResources().getString(R.string.connect_button));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.start_payment:
                mPaymentPresenter.startPayment(getActivity());
                break;
            case R.id.connect:
                mPaymentPresenter.operation();
                break;
            case R.id.d200:
                /*mD200.setBackgroundResource(R.drawable.edit_text_gray_background);
                mICMP.setBackgroundColor(Color.TRANSPARENT);
                mPaymentPresenter.initPayment(getActivity());*/
                break;
            case R.id.icmp:
                /*mICMP.setBackgroundResource(R.drawable.edit_text_gray_background);
                mD200.setBackgroundColor(Color.TRANSPARENT);
                mPaymentPresenter.initPayment(getActivity());*/
                break;
            case R.id.reverse_payment:
                mPaymentPresenter.reversalPayment();
                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        mPaymentPresenter.setSelectedDevice(parent.getItemAtPosition(position).toString());
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
