package payment.sunlinght.ru.sunlightpayment.view.payment;

import dagger.Component;
import payment.sunlinght.ru.sunlightpayment.view.FragmentScope;

/**
 * Created by sergey on 15.08.17.
 */
@FragmentScope
@Component(modules = PaymentModule.class)
public interface PaymentComponent {

    void inject(PaymentFragment fragment);
}
