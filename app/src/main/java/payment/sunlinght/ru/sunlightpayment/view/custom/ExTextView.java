package payment.sunlinght.ru.sunlightpayment.view.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.animation.Animation;

import payment.sunlinght.ru.sunlightpayment.R;


public class ExTextView extends AppCompatTextView {
    private boolean mIsClick = false;
    private Listener mListener;
    private Animation mAnimRotateDown;
    private Animation mAnimRotateRight;
    private boolean isExpended;

    public ExTextView(Context context) {
        super(context);

    }

    public ExTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        parseAttributes(context, attrs);
    }

    public ExTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        parseAttributes(context, attrs);
    }

    public void setListener(Listener listener) {
        mListener = listener;
    }

    private void parseAttributes(Context context, AttributeSet attrs) {
        TypedArray values = context.obtainStyledAttributes(attrs, R.styleable.ExTextView);
        int typeface = values.getInt(R.styleable.ExTextView_typeface, 0);
        setTypeface(FontsUtils.getTypeFace(context, typeface));
        values.recycle();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (mListener == null) {
            return super.onTouchEvent(event);
        }

        final float x = event.getX();
        final int width = getWidth();
        final int iconSize = getCompoundDrawables()[2] == null ? 0 : (int) (getCompoundDrawables()[2].getIntrinsicWidth() * 1.6f);
        if (x > width - iconSize) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                mIsClick = true;
            }
            if (event.getAction() == MotionEvent.ACTION_UP && mIsClick) {
                mListener.onRightIconClick(this);
//                getCompoundDrawables()[2].setLevel(0);
//                ObjectAnimator anim = ObjectAnimator.ofInt(getCompoundDrawables()[2], "level", 0, 10000);
//                anim.start();
            }
        } else {
            super.onTouchEvent(event);
        }

        return true;
    }

    public interface Listener {
        void onRightIconClick(ExTextView exTextView);

    }
}
