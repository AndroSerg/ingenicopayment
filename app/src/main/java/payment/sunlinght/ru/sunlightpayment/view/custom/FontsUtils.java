package payment.sunlinght.ru.sunlightpayment.view.custom;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;

import java.util.Hashtable;

public class FontsUtils {
    public final static int FUTURA_BOOK = 0;
    public final static int FUTURA_DEMI = 1;
    public final static int FUTURA_BOOK_ITALIC = 2;

    private static final Hashtable<String, Typeface> cache = new Hashtable<String, Typeface>();

    public static Typeface getTypeFace(Context context, int typeFace) {
        synchronized (cache) {
            String assetPath;
            switch (typeFace) {
                case FUTURA_BOOK:
                default:
                    assetPath = "fonts/FuturaPTBook.otf";
                    break;
                case FUTURA_DEMI:
                    assetPath = "fonts/FuturaPTDemi.otf";
                    break;
                case FUTURA_BOOK_ITALIC:
                    assetPath = "fonts/FuturaPTBookOblique.otf";
                    break;
            }
            if (!cache.containsKey(assetPath)) {
                try {
                    cache.put(assetPath, Typeface.createFromAsset(context.getAssets(), assetPath));
                } catch (Exception e) {
                    Log.e("TypeFaces", "Typeface not loaded.");
                    return null;
                }
            }
            return cache.get(assetPath);
        }
    }
}
