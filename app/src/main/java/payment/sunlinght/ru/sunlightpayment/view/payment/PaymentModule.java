package payment.sunlinght.ru.sunlightpayment.view.payment;


import dagger.Module;
import dagger.Provides;
import payment.sunlinght.ru.sunlightpayment.data.interactor.IPaymentInteractor;
import payment.sunlinght.ru.sunlightpayment.data.interactor.PaymentInteractor;

/**
 * Created by sergey on 15.08.17.
 */
@Module
public class PaymentModule {

    private IPaymentView mView;

    public PaymentModule(IPaymentView view){
        this.mView = view;
    }

    @Provides
    IPaymentInteractor provideInteractor(){
        return new PaymentInteractor();
    }

    @Provides
    IPaymentPresenter providePresenter(IPaymentInteractor interactor){
        return new PaymentPresenter(mView, interactor);
    }
}
