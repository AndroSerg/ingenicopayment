package payment.sunlinght.ru.sunlightpayment.view.payment;

import android.content.Context;

/**
 * Created by sergey on 15.08.17.
 */

public interface IPaymentPresenter {

    void initConnection(Context context);

    void initPayment(Context context);

    void setSelectedDevice(String device);

    void operation();

    void startPayment(Context context);

    void reversalPayment();
}
