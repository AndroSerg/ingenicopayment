package payment.sunlinght.ru.sunlightpayment.data.interactor;

import android.content.Context;
import android.util.Log;

import java.util.List;

import payment.sunlinght.ru.sunlightpayment.view.payment.IPaymentCallback;
import ru.m4bank.cardreaderlib.data.ReversalTransactionComponents;
import ru.m4bank.cardreaderlib.data.TransactionComponents;
import ru.m4bank.cardreaderlib.enums.TransactionType;
import ru.m4bank.cardreaderlib.manager.CardReader;
import ru.m4bank.cardreaderlib.manager.methods.output.CardReaderResponseExternalHandler;
import ru.m4bank.cardreaderlib.readers.host.TransactionCompletionData;
import ru.m4bank.connectionreaders.activate.ManagerStateReader;
import ru.m4bank.connectionreaders.activate.enums.ReaderType;
import ru.m4bank.connectionreaders.activate.handler.ConnectionReader;

/**
 * Created by sergey on 15.08.17.
 */

public class PaymentInteractor implements IPaymentInteractor, ConnectionReader, CardReaderResponseExternalHandler {

    private static ManagerStateReader managerStateReader;
    private static CardReader mCardReader;
    private IPaymentCallback callback;
    private boolean devices = false;
    private boolean mDeviceConnected = false;
    private String mTransactionId;
    private boolean connect = false;

    public PaymentInteractor() {

    }

    @Override
    public void setCallback(IPaymentCallback callback) {
        this.callback = callback;
    }

    @Override
    public void reconciliation() {
        if (mCardReader != null && mDeviceConnected) {
            mCardReader.reconciliation(this);
        }
    }

    @Override
    public void initConnection(Context context) {
        managerStateReader = new ManagerStateReader(context, this);
        ReaderType readerType = ReaderType.ICMP_UPOS;
        mCardReader = managerStateReader.activateReader(readerType);
    }

    @Override
    public void initPayment(Context context) {
        ReaderType readerType = ReaderType.ICMP_UPOS;
        mCardReader = managerStateReader.activateReader(readerType);
    }

    @Override
    public void operation(String device) {
        if (mDeviceConnected) {
            managerStateReader.stopReader();
        } else {
            if (mCardReader != null) {
                if (managerStateReader.getCardReader() != null) {
                    callback.statusMessage("Connection...");
                    managerStateReader.selectReaderFromList(device);
                } else {
                    ReaderType readerType = ReaderType.ICMP_UPOS;
                    connect = true;
                    mCardReader = managerStateReader.activateReader(readerType);
                }
            } else {
                ReaderType readerType = ReaderType.ICMP_UPOS;
                connect = true;
                mCardReader = managerStateReader.activateReader(readerType);
            }
        }
    }

    @Override
    public void startPayment(Context context, String deviceName, IPaymentCallback callback) {
        TransactionComponents transactionComponents = new TransactionComponents();
        transactionComponents.setCurrency("643"); // RUB
        transactionComponents.setCountryCode("643"); // Russia
        transactionComponents.setCurrencyExponent("2"); //
        transactionComponents.setAmount("3000");

        transactionComponents.setTransactionType(TransactionType.PAYMENT);
        transactionComponents.setMerchantId("744444445555");

        mCardReader.readCard(transactionComponents, this, () -> Log.d("Transaction2", "UnsupportedMethod"));
    }

    @Override
    public void cancelPayment() {
        managerStateReader.stopReader();
        managerStateReader.onReaderDisconnectedWithOutCallbacks();
    }

    @Override
    public void paymentReversal(String transId) {
        if (mCardReader != null && mDeviceConnected) {
            if (mTransactionId != null) {
                ReversalTransactionComponents reversalTransactionComponents = new ReversalTransactionComponents();
                reversalTransactionComponents.setMerchantId("744444445555");
                reversalTransactionComponents.setOperationNumber(mTransactionId);
                mCardReader.reversalOperation(this, reversalTransactionComponents);
            } else {
                mCardReader.revertLastOperation(this);
            }
        }
    }

    @Override
    public void onConnected(CardReader cardReader) {
        mDeviceConnected = true;
        callback.statusMessage("Connected");
        callback.deviceConnected();
        Log.d("Connection", "Success");
        Log.d("cardReader", Boolean.toString(cardReader.isConnected()));
    }

    @Override
    public void onDisconnected(CardReader cardReader) {
        mDeviceConnected = false;
        callback.statusMessage("Disconnected");
        callback.deviceDisconnected();
        Log.d("Connection", "Disconnected");
    }

    @Override
    public void onConnectedError(CardReader cardReader) {
        Log.d("Connection", "Error");
        callback.statusMessage("Connection Error");
        callback.deviceDisconnected();
    }

    @Override
    public void onDeviceList(List<String> list) {
        for (String device : list) Log.d("Device", device);
        callback.devices(list);
        if (connect && list.size() > 0) {
            connect = false;
            operation(list.get(0));
        }
    }

    @Override
    public void unsupportedMethod() {
        callback.statusMessage("Unsupported Method");
    }

    @Override
    public void onTransactionWithHostCompleted(TransactionCompletionData data) {
        callback.statusMessage(data.toString());
        Log.d("Payment", data.toString());
        if (data.getReceipt() != null) Log.d("Payment receipt", data.getReceipt());
        mTransactionId = data.getTransactionNumber();
    }
}
