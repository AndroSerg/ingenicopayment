package payment.sunlinght.ru.sunlightpayment.view.payment;

import java.util.List;

/**
 * Created by sergey on 15.08.17.
 */

public interface IPaymentCallback {

    void statusMessage(String message);

    void devices(List<String> devices);

    void deviceConnected();

    void deviceDisconnected();

    void paymentSuccess(String transId);
}
