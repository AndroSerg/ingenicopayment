package payment.sunlinght.ru.sunlightpayment.data.interactor;

import android.content.Context;

import payment.sunlinght.ru.sunlightpayment.view.payment.IPaymentCallback;

/**
 * Created by sergey on 15.08.17.
 */

public interface IPaymentInteractor {

    void initConnection(Context context);

    void initPayment(Context context);

    void operation(String device);

    void startPayment(Context context, String deviceName, IPaymentCallback callback);

    void setCallback(IPaymentCallback callback);

    void reconciliation();

    void cancelPayment();

    void paymentReversal(String transId);
}
