package ru.m4bank.cardreaderlib.manager.methods.output;

import java.util.List;

/**
 *
 * Callback handler for providing card reader connection process
 *
 */

public interface CardReaderConnectionHandler {
    
    /**
     * Called when card reader has connected
     *
     */

    public void onReaderConnected();
    
    /**
     * Called when card reader has disconnected
     *
     */

    public void onReaderDisconnected();
    
    /**
     * Called when card reader has disconnected and there is no need to call external callbacks
     *
     */

    public void onReaderDisconnectedWithOutCallbacks();
    
    /**
     * Called when connection error has occurred
     *
     */

    public void onReaderError();
    
    /**
     * Called when device list has available
     *
     * @param listDevice Available device list
     *
     */

    public void viewDeviceOfList(List<String> listDevice);
}
