package ru.m4bank.cardreaderlib.manager.methods.output;

import ru.m4bank.cardreaderlib.parser.readerinfo.ReaderInfo;

/**
 * Interface with get reader info operation status callbacks.
 *
 */

public interface ReaderInfoStatusTransactionHandler extends BaseStatusTransactionHandler{
    
    /**
     * Called when reader information arrived.
     *
     */

    public void success(ReaderInfo result);
    
}
