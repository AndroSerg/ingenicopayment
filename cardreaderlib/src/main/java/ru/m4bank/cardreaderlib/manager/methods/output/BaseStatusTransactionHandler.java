package ru.m4bank.cardreaderlib.manager.methods.output;

/**
 * Interface with transaction status callbacks.
 *
 */

public interface BaseStatusTransactionHandler {
    
    /**
     * Called when method is not supported.
     *
     */
    
    public void unsupportedMethod();
    
}
