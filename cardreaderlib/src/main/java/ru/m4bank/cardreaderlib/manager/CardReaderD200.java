package ru.m4bank.cardreaderlib.manager;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import ru.m4bank.cardreaderlib.data.ReversalTransactionComponents;
import ru.m4bank.cardreaderlib.data.TransactionComponents;
import ru.m4bank.cardreaderlib.enums.TransactionType;
import ru.m4bank.cardreaderlib.manager.methods.output.BaseStatusTransactionHandler;
import ru.m4bank.cardreaderlib.manager.methods.output.CardReaderConnectionHandler;
import ru.m4bank.cardreaderlib.manager.methods.output.CardReaderResponseExternalHandler;
import ru.m4bank.cardreaderlib.manager.methods.output.ReaderInfoStatusTransactionHandler;
import ru.m4bank.cardreaderlib.parser.readerinfo.ReaderInfo;
import ru.m4bank.cardreaderlib.parser.readerinfo.ReaderInfoD200;
import ru.m4bank.cardreaderlib.readers.allreader.converter.components.transaction.ConverterTransactionComponentsD200;
import ru.m4bank.cardreaderlib.readers.allreader.converter.device.search.selected.BluetoothDeviceStringConverter;
import ru.m4bank.cardreaderlib.readers.allreader.converter.device.search.service.DeviceSelectionServiceBluetooth;
import ru.m4bank.cardreaderlib.readers.host.HostReaderTransactionTypeConverter;
import ru.m4bank.cardreaderlib.readers.host.d200.D200TransactionCompletionDataConverter;
import ru.m4bank.util.d200lib.D200Controller;
import ru.m4bank.util.d200lib.ExternalD200CallbackReceiver;
import ru.m4bank.util.d200lib.dto.DeviceInformationData;
import ru.m4bank.util.d200lib.dto.TransactionResultData;

/**
 * D200 reader object.
 *
 */

public class CardReaderD200 extends CardReader implements ExternalD200CallbackReceiver {
    private D200Controller controller;
    private DeviceSelectionServiceBluetooth deviceSelectionService;
    private ReaderInfoStatusTransactionHandler readerInfoStatusTransactionHandler;

    private TransactionType transactionType;

    private CardReaderD200(Context context, CardReaderConnectionHandler cardReaderConnectionHandler) {
        super(context, cardReaderConnectionHandler);
    }
    
    /**
     * Create object {@link CardReaderD200} class.
     *
     * @param context The context
     * @param cardReaderConnectionHandler Callback handler for providing card reader connection process
     */
    
    public static CardReaderD200 getNewInstance(Context context, CardReaderConnectionHandler cardReaderConnectionHandler) {
        CardReaderD200 cardReaderD200 = new CardReaderD200(context, cardReaderConnectionHandler);
        cardReaderD200.controller = new D200Controller(cardReaderD200);
        cardReaderD200.controller.init();
        return cardReaderD200;
    }
    
    /**
     * Start card reader connection process.
     *
     */
    
    @Override
    public void connect() {
        List<BluetoothDevice> discoveredDevices = new ArrayList<BluetoothDevice>();
        Set<BluetoothDevice> boundedDevices = BluetoothAdapter.getDefaultAdapter()
                .getBondedDevices();
        for (BluetoothDevice device : boundedDevices) {
            if (device.getName().startsWith("D200")) {
                discoveredDevices.add(device);
            }
        }
        deviceSelectionService = new DeviceSelectionServiceBluetooth(discoveredDevices, new BluetoothDeviceStringConverter());
        List<String> listDeviceName = deviceSelectionService.getSelectionList();
        viewDeviceOfList(listDeviceName);
    }
    
    /**
     * Select reader by device name.
     *
     * @param selectDevice device name for selection
     */
    
    @Override
    public void selectedReader(String selectDevice) {
        if (deviceSelectionService != null) {
            controller.connect(deviceSelectionService.getObject(selectDevice));
        }
    }
    
    /**
     * Disconnect from card reader.
     *
     */
    
    @Override
    public void disconnect() {
        if (statusConnect) {
            controller.disconnect();
        }
    }
    
    /**
     * Make get information operation.
     *
     * @param readerInfoStatusTransactionHandler reader info status callback hundler
     */
    
    @Override
    public void getCardReaderInformation(ReaderInfoStatusTransactionHandler readerInfoStatusTransactionHandler) {
        this.readerInfoStatusTransactionHandler = readerInfoStatusTransactionHandler;
        controller.getInformation();
    }
    
    /**
     * Make reversal operation.
     *
     * @param cardReaderResponseExternalHandler reader status callback handler
     * @param reversalTransactionComponents necessary components for reversal transaction
     */
    
    @Override
    public void reversalOperation(CardReaderResponseExternalHandler cardReaderResponseExternalHandler, ReversalTransactionComponents reversalTransactionComponents) {
        cardReaderResponseExternalHandler.unsupportedMethod();
    }
    
    /**
     * Make payment or refund operation depends of transactionType in {@link TransactionComponents}.
     *
     * @param transactionComponents necessary transaction components (amount, transactionType e. t. c.)
     * @param cardReaderResponseExternalHandler card reader response callbacks handler
     * @param baseStatusTransactionHandler transaction status response callbacks handler
     */
    
    @Override
    public void readCard(TransactionComponents transactionComponents, CardReaderResponseExternalHandler cardReaderResponseExternalHandler, BaseStatusTransactionHandler baseStatusTransactionHandler) {
        super.readCard(transactionComponents, cardReaderResponseExternalHandler, baseStatusTransactionHandler);
        transactionType = transactionComponents.getTransactionType();
        controller.makeTransaction(new ConverterTransactionComponentsD200().createTransactionComponents(transactionComponents));
    }
    
    /**
     * Make reconciliation operation.
     *
     * @param cardReaderResponseExternalHandler reader info status callback hundler
     */
    
    @Override
    public void reconciliation(CardReaderResponseExternalHandler cardReaderResponseExternalHandler) {
        super.reconciliation(cardReaderResponseExternalHandler);
        reconciliation();
    }
    
    /**
     * Make reconciliation operation.
     *
     */
    
    private void reconciliation() {
        TransactionComponents transactionComponents = new TransactionComponents();
        transactionComponents.setAmount("0");
        transactionComponents.setTransactionType(TransactionType.RECONCILIATION);
        transactionType = transactionComponents.getTransactionType();
        controller.makeTransaction(new ConverterTransactionComponentsD200().createTransactionComponents(transactionComponents));
    }
    
    /**
     * Make revert last operation.
     *
     */
    
    @Override
    public void revertLastOperation() {
        TransactionComponents transactionComponents = new TransactionComponents();
        transactionComponents.setAmount("0");
        transactionComponents.setTransactionType(TransactionType.CANCEL);
        transactionType = transactionComponents.getTransactionType();
        controller.makeTransaction(new ConverterTransactionComponentsD200().createTransactionComponents(transactionComponents));
    }
    
    /**
     * Make revert last operation.
     *
     * @param cardReaderResponseExternalHandler reader status callback handler
     */
    
    @Override
    public void revertLastOperation(CardReaderResponseExternalHandler cardReaderResponseExternalHandler) {
        super.revertLastOperation(cardReaderResponseExternalHandler);
        revertLastOperation();
    }
    
    /**
     * Make enter service menu operation.
     *
     * @param cardReaderResponseExternalHandler reader status callback handler
     */
    
    @Override
    public void enterServiceMenu(CardReaderResponseExternalHandler cardReaderResponseExternalHandler) {
        super.enterServiceMenu(cardReaderResponseExternalHandler);
        enterServiceMenu();
    }
    
    /**
     * Make enter service menu operation.
     *
     */
    
    public void enterServiceMenu() {
        TransactionComponents transactionComponents = new TransactionComponents();
        transactionComponents.setAmount("0");
        transactionComponents.setTransactionType(TransactionType.ENTER_SERVICE_MENU);
        transactionType = transactionComponents.getTransactionType();
        controller.makeTransaction(new ConverterTransactionComponentsD200().createTransactionComponents(transactionComponents));
    }
    
    /**
     * Calls corresponded callback when reader has connected
     *
     */
    
    @Override
    public void onConnected() {
        onReaderConnected();
    }
    
    /**
     * Calls corresponded callback when reader error has occurred
     *
     */
    
    @Override
    public void onConnectionError(String s) {
        controller.disconnect();
        onReaderError();
    }
    
    /**
     * Calls card reader error callback when reader error in connection process has occurred
     *
     */
    
    
    @Override
    public void onConnectionProcessInterrupted() {
        onReaderError();
    }
    
    /**
     * Calls corresponded callback when reader has disconnected
     *
     */
    
    @Override
    public void onDisconnected() {
        onReaderDisconnected();
    }
    
    /**
     * Calls success callback in reader information status handler when device information has arrived
     *
     */
    
    @Override
    public void onDeviceInformationReceived(DeviceInformationData deviceInformationData) {
        ReaderInfo readerInfo = new ReaderInfoD200();
        readerInfo.createReaderVersion(deviceInformationData);
        readerInfoStatusTransactionHandler.success(readerInfo);
    }
    
    /**
     * Calls onTransactionWithHostCompleted callback in reader response handler when transaction has completed
     *
     */
    
    @Override
    public void onTransactionCompleted(TransactionResultData transactionResultData) {
        cardReaderResponseExternalHandler.onTransactionWithHostCompleted(new D200TransactionCompletionDataConverter().convert(transactionResultData,
                new HostReaderTransactionTypeConverter().convert(transactionType)));
    }
}
