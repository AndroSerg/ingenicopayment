package ru.m4bank.cardreaderlib.manager;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import ru.m4bank.cardreaderlib.data.ReversalTransactionComponents;
import ru.m4bank.cardreaderlib.data.TransactionComponents;
import ru.m4bank.cardreaderlib.manager.methods.input.Controller;
import ru.m4bank.cardreaderlib.manager.methods.output.BaseStatusTransactionHandler;
import ru.m4bank.cardreaderlib.manager.methods.output.CardReaderConnectionHandler;
import ru.m4bank.cardreaderlib.manager.methods.output.CardReaderResponseExternalHandler;
import ru.m4bank.cardreaderlib.manager.methods.output.ReaderInfoStatusTransactionHandler;
import ru.m4bank.cardreaderlib.parser.readerinfo.ReaderInfo;

/**
 * Common reader object.
 *
 */

public abstract class CardReader implements CardReaderConnectionHandler, Controller {

    protected Context context;
    protected ReaderInfo readerInfo;
    protected List<String> parserError;

    protected CardReaderResponseExternalHandler cardReaderResponseExternalHandler;
    private CardReaderConnectionHandler cardReaderConnectionHandler;

    protected BaseStatusTransactionHandler baseStatusTransactionHandler; //проведение картой

    protected boolean statusConnect = false;

    public CardReader(Context context, CardReaderConnectionHandler cardReaderConnectionHandler) {
        this.context = context;
        this.cardReaderConnectionHandler = cardReaderConnectionHandler;
        statusConnect = false;
    }

    //Controller methods
    
    /**
     * Connect to card reader.
     *
     */
    
    @Override
    public void connect() {
        parserError = new ArrayList<String>();
    }
    
    /**
     * Make payment or refund operation depends of transactionType in {@link TransactionComponents}.
     *
     * @param transactionComponents necessary transaction components (amount, transactionType e. t. c.)
     * @param cardReaderResponseExternalHandler card reader response callbacks handler
     * @param baseStatusTransactionHandler transaction status response callbacks handler
     */
    
    @Override
    public void readCard(TransactionComponents transactionComponents, CardReaderResponseExternalHandler cardReaderResponseExternalHandler, BaseStatusTransactionHandler baseStatusTransactionHandler) {
        this.cardReaderResponseExternalHandler = cardReaderResponseExternalHandler;
        this.baseStatusTransactionHandler = baseStatusTransactionHandler;
    }
    
    /**
     * Make get information operation.
     *
     * @param readerInfoStatusTransactionHandler reader info status callback handler
     */
    
    @Override
    public void getCardReaderInformation(ReaderInfoStatusTransactionHandler readerInfoStatusTransactionHandler) {
        readerInfoStatusTransactionHandler.unsupportedMethod();
    }
    
    /**
     * Make reconciliation operation.
     *
     * @param cardReaderResponseExternalHandler reader info status callback handler
     */
    
    @Override
    public void reconciliation(CardReaderResponseExternalHandler cardReaderResponseExternalHandler) {
        this.cardReaderResponseExternalHandler = cardReaderResponseExternalHandler;
    }
    
    /**
     * Make revert last operation.
     *
     */
    
    @Override
    public void revertLastOperation() {

    }
    
    /**
     * Make revert last operation.
     *
     * @param cardReaderResponseExternalHandler reader status callback handler
     */
    
    @Override
    public void revertLastOperation(CardReaderResponseExternalHandler cardReaderResponseExternalHandler) {
        this.cardReaderResponseExternalHandler = cardReaderResponseExternalHandler;
    }
    
    /**
     * Make reversal operation.
     *
     * @param cardReaderResponseExternalHandler reader status callback handler
     * @param reversalTransactionComponents necessary components for reversal transaction
     */
    
    @Override
    public void reversalOperation(CardReaderResponseExternalHandler cardReaderResponseExternalHandler, ReversalTransactionComponents reversalTransactionComponents) {
        this.cardReaderResponseExternalHandler = cardReaderResponseExternalHandler;
    }
    
    /**
     * Make enter service menu operation.
     *
     * @param cardReaderResponseExternalHandler reader status callback handler
     */
    
    @Override
    public void enterServiceMenu(CardReaderResponseExternalHandler cardReaderResponseExternalHandler) {
        this.cardReaderResponseExternalHandler = cardReaderResponseExternalHandler;
    }
    
    /**
     * Disconnect from card reader.
     *
     */
    
    @Override
    public void disconnect() {

    }
    
    /**
     * Disconnect from card reader without callbacks.
     *
     */
    
    @Override
    public void disconnectWithOutCallBacks() {
        onReaderDisconnectedWithOutCallbacks();
        disconnect();
    }
    
    /**
     * Select reader by device name.
     *
     * @param selectDevice device name for selection
     */
    
    @Override
    public void selectedReader(String selectDevice) {

    }

    //CardReaderConnectionHandler methods
    
    /**
     * Called when card reader has connected
     *
     */
    
    @Override
    public void onReaderConnected() {
        statusConnect = true;
        if(cardReaderConnectionHandler != null)
            cardReaderConnectionHandler.onReaderConnected();
    }
    
    /**
     * Called when card reader has disconnected
     *
     */

    @Override
    public void onReaderDisconnected() {
        statusConnect = false;
        if(cardReaderConnectionHandler != null)
            cardReaderConnectionHandler.onReaderDisconnected();
    }
    
    /**
     * Called when card reader has disconnected and there is no need to call external callbacks
     *
     */

    @Override
    public void onReaderDisconnectedWithOutCallbacks() {
        if (cardReaderConnectionHandler != null) {
            cardReaderConnectionHandler.onReaderDisconnectedWithOutCallbacks();
            cardReaderConnectionHandler = null;
        }
    }
    
    /**
     * Called when connection error has occurred
     *
     */

    @Override
    public void onReaderError() {
        statusConnect = false;
        if(cardReaderConnectionHandler != null)
            cardReaderConnectionHandler.onReaderError();
    }
    
    /**
     * Called when device list has available
     *
     * @param listDevice Available device list
     *
     */
    
    @Override
    public void viewDeviceOfList(List<String> listDevice) {
        if(cardReaderConnectionHandler != null)
            cardReaderConnectionHandler.viewDeviceOfList(listDevice);
    }
    
    /**
     * Check reader connection status
     *
     * @return Return true if reader is connected otherwise return false
     *
     */

    public boolean isConnected() {
        return statusConnect;
    }
    
}
