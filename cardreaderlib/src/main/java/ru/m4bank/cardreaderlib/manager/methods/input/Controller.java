package ru.m4bank.cardreaderlib.manager.methods.input;

import ru.m4bank.cardreaderlib.data.ReversalTransactionComponents;
import ru.m4bank.cardreaderlib.data.TransactionComponents;
import ru.m4bank.cardreaderlib.manager.methods.output.BaseStatusTransactionHandler;
import ru.m4bank.cardreaderlib.manager.methods.output.CardReaderResponseExternalHandler;
import ru.m4bank.cardreaderlib.manager.methods.output.ReaderInfoStatusTransactionHandler;

/**
 * Interface with general card reader methods.
 *
 */

public interface Controller {
	
	/**
	 * Connect to card reader.
	 *
	 */
	
	void connect();
	
	/**
	 * Disconnect from card reader.
	 *
	 */

	void disconnect();
	
	/**
	 * Disconnect from card reader without callbacks.
	 *
	 */

	void disconnectWithOutCallBacks();
	
	/**
	 * Make payment or refund operation depends of transactionType in {@link TransactionComponents}.
	 *
	 * @param transactionComponents necessary transaction components (amount, transactionType e. t. c.)
	 * @param cardReaderResponseHandler card reader response callbacks handler
	 * @param baseStatusTransactionHandler transaction status response callbacks handler
	 */

	void readCard(TransactionComponents transactionComponents, CardReaderResponseExternalHandler cardReaderResponseHandler, BaseStatusTransactionHandler baseStatusTransactionHandler);
	
	/**
	 * Make get information operation.
	 *
	 * @param readerInfoStatusTransactionHandler reader info status callback hundler
	 */
	
	void getCardReaderInformation(ReaderInfoStatusTransactionHandler readerInfoStatusTransactionHandler);
	
	/**
	 * Make reconciliation operation.
	 *
	 * @param cardReaderResponseExternalHandler reader info status callback hundler
	 */
	
	void reconciliation(CardReaderResponseExternalHandler cardReaderResponseExternalHandler);
	
	/**
	 * Make revert last operation.
	 *
	 */

	void revertLastOperation();
	
	/**
	 * Make revert last operation.
	 *
	 * @param cardReaderResponseExternalHandler reader status callback handler
	 */

	void revertLastOperation(CardReaderResponseExternalHandler cardReaderResponseExternalHandler);
	
	/**
	 * Make reversal operation.
	 *
	 * @param cardReaderResponseExternalHandler reader status callback handler
	 * @param reversalTransactionComponents necessary components for reversal transaction
	 */

	void reversalOperation(CardReaderResponseExternalHandler cardReaderResponseExternalHandler, ReversalTransactionComponents reversalTransactionComponents);
	
	/**
	 * Make enter service menu operation.
	 *
	 * @param cardReaderResponseExternalHandler reader status callback handler
	 */
	
	void enterServiceMenu(CardReaderResponseExternalHandler cardReaderResponseExternalHandler);
	
	/**
	 * Select reader by device name.
	 *
	 * @param selectDevice device name for selection
	 */

	void selectedReader(String selectDevice);

}