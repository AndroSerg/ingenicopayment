package ru.m4bank.cardreaderlib.manager.methods.output;

import ru.m4bank.cardreaderlib.readers.host.TransactionCompletionData;

/**
 * Interface with card reader response callbacks.
 *
 */

public interface CardReaderResponseExternalHandler extends BaseStatusTransactionHandler{
	
	/**
	 * Called when transaction with host completed.
	 *
	 */
	
	void onTransactionWithHostCompleted(TransactionCompletionData data);
	
}
