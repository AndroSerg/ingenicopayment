package ru.m4bank.cardreaderlib.manager;

import android.content.Context;

import java.util.List;

import m4bank.ru.icmplibrary.ExternalIcmpCallbackReceiver;
import m4bank.ru.icmplibrary.IcmpController;
import m4bank.ru.icmplibrary.dto.InformationResultData;
import m4bank.ru.icmplibrary.dto.TransactionResultData;
import ru.m4bank.cardreaderlib.data.ReversalTransactionComponents;
import ru.m4bank.cardreaderlib.data.TransactionComponents;
import ru.m4bank.cardreaderlib.enums.TransactionType;
import ru.m4bank.cardreaderlib.manager.methods.output.BaseStatusTransactionHandler;
import ru.m4bank.cardreaderlib.manager.methods.output.CardReaderConnectionHandler;
import ru.m4bank.cardreaderlib.manager.methods.output.CardReaderResponseExternalHandler;
import ru.m4bank.cardreaderlib.manager.methods.output.ReaderInfoStatusTransactionHandler;
import ru.m4bank.cardreaderlib.parser.readerinfo.ReaderInfo;
import ru.m4bank.cardreaderlib.parser.readerinfo.ReaderInfoIcmp;
import ru.m4bank.cardreaderlib.readers.allreader.converter.components.transaction.ConverterTransactionComponentsIcmp;
import ru.m4bank.cardreaderlib.readers.host.HostReaderTransactionTypeConverter;
import ru.m4bank.cardreaderlib.readers.host.TransactionCompletionData;
import ru.m4bank.cardreaderlib.readers.host.icmp.IcmpTransactionCompletionDataConverter;

/**
 * ICMP reader object.
 *
 */

public class CardReaderIcmp extends CardReader implements ExternalIcmpCallbackReceiver {
    private IcmpController controller;
    private ReaderInfoStatusTransactionHandler readerInfoStatusTransactionHandler;

    private TransactionType transactionType;

    private CardReaderIcmp(Context context, CardReaderConnectionHandler cardReaderConnectionHandler) {
        super(context, cardReaderConnectionHandler);
    }
    
    /**
     * Create object {@link CardReaderIcmp} class.
     *
     * @param context The context
     * @param cardReaderConnectionHandler Callback handler for providing card reader connection process
     */
    
    public static CardReaderIcmp getNewInstance(Context context, CardReaderConnectionHandler cardReaderConnectionHandler) {
        CardReaderIcmp cardReaderIcmp = new CardReaderIcmp(context, cardReaderConnectionHandler);
        cardReaderIcmp.controller = new IcmpController();
        cardReaderIcmp.controller.init(context, cardReaderIcmp);
        return cardReaderIcmp;
    }
    
    /**
     * Start card reader connection process.
     *
     */
    
    @Override
    public void connect() {
        controller.connect();
    }
    
    /**
     * Select reader by device name.
     *
     * @param selectDevice device name for selection
     */

    @Override
    public void selectedReader(String selectDevice) {
        controller.selectDeviceFromList(selectDevice);
    }
    
    /**
     * Disconnect from card reader.
     *
     */

    @Override
    public void disconnect() {
        controller.disconnect();
    }
    
    /**
     * Make get information operation.
     *
     * @param readerInfoStatusTransactionHandler reader info status callback handler
     */

    @Override
    public void getCardReaderInformation(ReaderInfoStatusTransactionHandler readerInfoStatusTransactionHandler) {
        this.readerInfoStatusTransactionHandler = readerInfoStatusTransactionHandler;
        controller.getInformation();
    }
    
    /**
     * Make payment or refund operation depends of transactionType in {@link TransactionComponents}.
     *
     * @param transactionComponents necessary transaction components (amount, transactionType e. t. c.)
     * @param cardReaderResponseExternalHandler card reader response callbacks handler
     * @param baseStatusTransactionHandler transaction status response callbacks handler
     */
    
    @Override
    public void readCard(TransactionComponents transactionComponents, CardReaderResponseExternalHandler cardReaderResponseExternalHandler, BaseStatusTransactionHandler baseStatusTransactionHandler) {
        super.readCard(transactionComponents, cardReaderResponseExternalHandler, baseStatusTransactionHandler);
        transactionType = transactionComponents.getTransactionType();
        controller.makeTransaction(new ConverterTransactionComponentsIcmp().createTransactionComponents(transactionComponents));
    }
    
    /**
     * Make reconciliation operation.
     *
     * @param cardReaderResponseExternalHandler reader info status callback handler
     */
    
    @Override
    public void reconciliation(CardReaderResponseExternalHandler cardReaderResponseExternalHandler) {
        super.reconciliation(cardReaderResponseExternalHandler);
        this.cardReaderResponseExternalHandler = cardReaderResponseExternalHandler;
        reconciliation();
    }
    
    /**
     * Make reconciliation operation.
     *
     */
    
    public void reconciliation() {
        TransactionComponents transactionComponents = new TransactionComponents();
        transactionComponents.setTransactionType(TransactionType.RECONCILIATION);
        transactionType = transactionComponents.getTransactionType();
        controller.makeTransaction(new ConverterTransactionComponentsIcmp().createTransactionComponents(transactionComponents));
    }
    
    /**
     * Make revert last operation.
     *
     */
    
    @Override
    public void revertLastOperation() {
        TransactionComponents transactionComponents = new TransactionComponents();
        transactionComponents.setTransactionType(TransactionType.REVERSAL_OF_LAST);
        transactionType = transactionComponents.getTransactionType();
        controller.makeTransaction(new ConverterTransactionComponentsIcmp().createTransactionComponents(transactionComponents));
    }
    
    /**
     * Make revert last operation.
     *
     * @param cardReaderResponseExternalHandler reader status callback handler
     */
    
    @Override
    public void revertLastOperation(CardReaderResponseExternalHandler cardReaderResponseExternalHandler) {
        super.revertLastOperation(cardReaderResponseExternalHandler);
        revertLastOperation();
    }
    
    /**
     * Make reversal operation.
     *
     * @param cardReaderResponseExternalHandler reader status callback handler
     * @param reversalTransactionComponents necessary components for reversal transaction
     */
    
    @Override
    public void reversalOperation(CardReaderResponseExternalHandler cardReaderResponseExternalHandler, ReversalTransactionComponents reversalTransactionComponents) {
        super.reversalOperation(cardReaderResponseExternalHandler, reversalTransactionComponents);
        TransactionComponents transactionComponents = new TransactionComponents();
        transactionComponents.setTransactionType(TransactionType.CANCEL);
        if(reversalTransactionComponents != null && reversalTransactionComponents.getOperationNumber() != null)
            transactionComponents.setRrn(reversalTransactionComponents.getOperationNumber());
        if(reversalTransactionComponents != null && reversalTransactionComponents.getMerchantId() != null)
            transactionComponents.setMerchantId(reversalTransactionComponents.getMerchantId());
        transactionType = transactionComponents.getTransactionType();
        controller.makeTransaction(new ConverterTransactionComponentsIcmp().createTransactionComponents(transactionComponents));
    }
    
    /**
     * Make enter service menu operation.
     *
     * @param cardReaderResponseExternalHandler reader status callback handler
     */
    
    @Override
    public void enterServiceMenu(CardReaderResponseExternalHandler cardReaderResponseExternalHandler) {
        super.enterServiceMenu(cardReaderResponseExternalHandler);
        enterServiceMenu();
    }
    
    /**
     * Make enter service menu operation.
     *
     */
    
    public void enterServiceMenu() {
        TransactionComponents transactionComponents = new TransactionComponents();
        transactionComponents.setTransactionType(TransactionType.ENTER_SERVICE_MENU);
        transactionType = transactionComponents.getTransactionType();
        controller.makeTransaction(new ConverterTransactionComponentsIcmp().createTransactionComponents(transactionComponents));
    }
    
    /**
     * Calls corresponded callback when reader has connected
     *
     */
    
    @Override
    public void onConnected() {
        onReaderConnected();
    }
    
    /**
     * Calls corresponded callback when reader error has occurred
     *
     */
    
    @Override
    public void onConnectionError(String s) {
        controller.disconnect();
        onReaderError();
    }
    
    /**
     * Calls card reader error callback when reader error in connection process has occurred
     *
     */
    
    @Override
    public void onConnectionProcessInterrupted() {
        onReaderError();
    }
    
    /**
     * Calls corresponded callback when reader has disconnected
     *
     */
    
    @Override
    public void onDisconnected() {
        onReaderDisconnected();
    }
    
    /**
     * Called when device list has available
     *
     * @param list Available device list
     *
     */
    
    @Override
    public void onDeviceList(List<String> list) {
        viewDeviceOfList(list);
    }
    
    /**
     * Calls success callback in reader information status handler when device information has arrived
     *
     */
    
    @Override
    public void onDeviceInformationReceived(InformationResultData informationResultData) {
        ReaderInfo readerInfo = new ReaderInfoIcmp();
        readerInfo.createReaderVersion(informationResultData);
        readerInfo.createSerialNumber(informationResultData);
        readerInfoStatusTransactionHandler.success(readerInfo);
    }
    
    /**
     * Calls onTransactionWithHostCompleted callback in reader response handler when transaction has completed successfully
     *
     */
    
    @Override
    public void onTransactionCompleted(TransactionResultData transactionResultData) {
        cardReaderResponseExternalHandler.onTransactionWithHostCompleted(new IcmpTransactionCompletionDataConverter().convert(transactionResultData,
                new HostReaderTransactionTypeConverter().convert(transactionType)));
    }
    
    /**
     * Calls onTransactionWithHostCompleted callback in reader response handler when transaction has completed and decline
     *
     */
    
    @Override
    public void onTransactionDecline(TransactionResultData transactionResultData) {
        cardReaderResponseExternalHandler.onTransactionWithHostCompleted(new IcmpTransactionCompletionDataConverter().convert(transactionResultData,
                new HostReaderTransactionTypeConverter().convert(transactionType)));
    }
    
    /**
     * Calls onTransactionWithHostCompleted callback in reader response handler when the error has occurred in transaction process
     *
     */
    
    @Override
    public void onTransactionError(String s) {
        if(isConnected())
            cardReaderResponseExternalHandler.onTransactionWithHostCompleted(new TransactionCompletionData.Builder().build());
    }
}
