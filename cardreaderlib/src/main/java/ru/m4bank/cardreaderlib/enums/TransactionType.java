package ru.m4bank.cardreaderlib.enums;

/**
 * Transaction types.
 *
 */

public enum TransactionType {
    UNKNOWN(""),
    PAYMENT("payment"),
    REFUND("refund"),
    CANCEL("cancel"),
    REVERSAL_OF_LAST("reversalOfLast"),
    RECONCILIATION("reconciliation"),
    ENTER_SERVICE_MENU("enterServiceMenu");

    private final String code;

    TransactionType(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public static TransactionType getByCode(String code) {
        for (TransactionType transactionType : TransactionType.values()) {
            if (transactionType.getCode().equals(code)) {
                return transactionType;
            }
        }
        return UNKNOWN;
    }
}
