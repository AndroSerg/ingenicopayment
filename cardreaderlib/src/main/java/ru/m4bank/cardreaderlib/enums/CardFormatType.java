package ru.m4bank.cardreaderlib.enums;

/**
 * Payment service provider types.
 *
 */

public enum CardFormatType {

    DEFAULT(""),
    VISA("VISA"),
    MASTER_CARD("MASTERCARD"),
    MAESTRO("MAESTRO"),
    AMEX("AMEX"),
    CHINA_UNION_PAY("CHUP"),
    TCARD("TCARD"),
    UZCARD("UZCARD"),
    BELCARD("BELCARD");

    private final String code;

    CardFormatType(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public static CardFormatType getByCode(String code) {
        for (CardFormatType s : CardFormatType.values()) {
            if (s.getCode().equals(code)) {
                return s;
            }
        }
        return DEFAULT;
    }
}
