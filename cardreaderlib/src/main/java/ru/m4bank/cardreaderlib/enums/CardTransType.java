package ru.m4bank.cardreaderlib.enums;

/**
 * Card operation types.
 *
 */

public enum CardTransType {
    UNKNOWN("MANUAL"),
    CONTACT_EMV("EMV"),
    CONTACTLESS_EMV("NFC"),
    MAGNETIC_STRIPE("TRACK");

    private final String code;

    CardTransType(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public static CardTransType getByCode(String code) {
        for (CardTransType cardTransType : CardTransType.values()) {
            if (cardTransType.getCode().equals(code)) {
                return cardTransType;
            }
        }
        return UNKNOWN;
    }
}
