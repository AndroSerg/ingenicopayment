package ru.m4bank.cardreaderlib.parser.readerinfo;

import ru.m4bank.util.d200lib.dto.DeviceInformationData;

/**
 * D200 reader info data.
 *
 */

public class ReaderInfoD200 extends ReaderInfo {
    
    /**
     * Get reader version.
     *
     * @param info The information object for specified reader type
     *
     * @return Return string value of reader version
     */
    
    @Override
    public String createReaderVersion(Object info) {
        return readerVersion = ((DeviceInformationData) info).getDeviceInformation();
    }
    
}
