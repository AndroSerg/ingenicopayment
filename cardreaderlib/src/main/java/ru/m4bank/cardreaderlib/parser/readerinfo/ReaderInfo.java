package ru.m4bank.cardreaderlib.parser.readerinfo;

/**
 * Common card reader info data.
 *
 */

public class ReaderInfo {

    protected String readerVersion = null;

    protected String serialNumber = null;

    protected String ksn = null;
    
    /**
     * Get reader version.
     *
     * @param info The information object for specified reader type
     *
     * @return Return string value of reader version
     */
    
    public String createReaderVersion (Object info) {
        readerVersion = null;
        return readerVersion;
    }
    
    /**
     * Get reader version.
     *
     * @param info The information object for specified reader type
     *
     * @return Return string value of serial number
     */

    public String createSerialNumber (Object info) {
        serialNumber = null;
        return serialNumber;
    }

    public String createKsn (Object info) {
        ksn = null;
        return ksn;
    }

    public String getReaderVersion() {
        return readerVersion;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public String getKsn() {
        return ksn;
    }
}
