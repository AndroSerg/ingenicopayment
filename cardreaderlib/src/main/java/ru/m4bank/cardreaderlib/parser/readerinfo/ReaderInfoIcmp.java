package ru.m4bank.cardreaderlib.parser.readerinfo;

import m4bank.ru.icmplibrary.dto.InformationResultData;

/**
 * ICMP reader info data.
 *
 */

public class ReaderInfoIcmp extends ReaderInfo {
    
    /**
     * Get reader version.
     *
     * @param info The information object for specified reader type
     *
     * @return Return string value of reader version
     */
    
    @Override
    public String createReaderVersion(Object info) {
        return readerVersion = ((InformationResultData) info).getPn();
    }
    
    /**
     * Get reader version.
     *
     * @param info The information object for specified reader type
     *
     * @return Return string value of serial number
     */

    public String createSerialNumber (Object info) {
        return serialNumber = ((InformationResultData) info).getSn();
    }
}
