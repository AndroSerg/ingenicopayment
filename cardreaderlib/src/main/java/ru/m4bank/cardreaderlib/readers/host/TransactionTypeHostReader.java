package ru.m4bank.cardreaderlib.readers.host;

/**
 * Common transaction types.
 *
 */

public enum TransactionTypeHostReader {
    UNKNOWN(""),
    PAYMENT("payment"),
    REFUND("refund"),
    CANCEL("cancel"),
    REVERSAL_OF_LAST("reversalOfLast"),
    RECONCILIATION("reconciliation");

    private final String code;

    TransactionTypeHostReader(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public static TransactionTypeHostReader getByCode(String code) {
        for (TransactionTypeHostReader transactionType : TransactionTypeHostReader.values()) {
            if (transactionType.getCode().equals(code)) {
                return transactionType;
            }
        }
        return UNKNOWN;
    }
}
