package ru.m4bank.cardreaderlib.readers.allreader.converter.components.transaction;

import m4bank.ru.icmplibrary.dto.TransactionInputData;
import m4bank.ru.icmplibrary.operation.enums.transaction.IcmpOperationType;
import ru.m4bank.cardreaderlib.data.TransactionComponents;

/**
 * Converter transaction components for ICMP.
 *
 */

public class ConverterTransactionComponentsIcmp extends ConverterTransactionComponents<TransactionInputData> {
    
    /**
     * Convert transaction components into {@link TransactionInputData} class.
     * @param transactionComponents transaction components
     */
    
    @Override
    public TransactionInputData createTransactionComponents(TransactionComponents transactionComponents) {
        TransactionInputData transactionInputData = new TransactionInputData();
        String amount = transactionComponents.getAmount();
        String currencyCode = transactionComponents.getCurrency();
        String operationNumber = transactionComponents.getRrn();

        IcmpOperationType operationType;
        switch (transactionComponents.getTransactionType()) {
            case PAYMENT:
                operationType = IcmpOperationType.PAYMENT;
                break;
            case CANCEL:
                operationType = IcmpOperationType.REVERSAL;
                break;
            case REVERSAL_OF_LAST:
                operationType = IcmpOperationType.REVERSAL_LAST;
                break;
            case REFUND:
                operationType = IcmpOperationType.REFUND;
                break;
            case RECONCILIATION:
                operationType = IcmpOperationType.RECONCILIATION;
                break;
            case ENTER_SERVICE_MENU:
                operationType = IcmpOperationType.ENTER_SERVICE_MENU;
                break;
            default:
                operationType = IcmpOperationType.RECONCILIATION;
                break;
        }
        transactionInputData.setAmount(amount);
        transactionInputData.setCurrencyCode(currencyCode);
        transactionInputData.setIcmpOperationType(operationType);
        if(operationNumber != null && !"".equals(operationNumber)) {
            try {
                transactionInputData.setOperationNumber(operationNumber);
            } catch(Exception e) {
                e.printStackTrace();
            }
        }
        transactionInputData.setMerchandId(transactionComponents.getMerchantId());

        return transactionInputData;
    }
}
