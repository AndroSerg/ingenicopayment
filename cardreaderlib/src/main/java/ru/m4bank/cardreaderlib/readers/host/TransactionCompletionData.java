package ru.m4bank.cardreaderlib.readers.host;

import java.util.Date;

import ru.m4bank.cardreaderlib.enums.CardTransType;

/**
 * Common transaction data.
 *
 */

public class TransactionCompletionData {
    private final int resultCode;
    private final String authorizationCode;
    private final String rrn;
    private final String transactionNumber;
    private final String maskedCardNumber;
    private final String cardExpiryDate;
    private final String errorMessage;
    private final Date operationDateTime;
    private final String terminalNumber;
    private final String paymentSystemName;
    private final String cardNumberHash;
    private final int requestId;
    private final TransactionTypeHostReader transactionTypeHostReader;
    private final CardTransType cardEntryType;
    private final boolean pinEntered;
    private final boolean sign;
    private final String receipt;
    
    private TransactionCompletionData(TransactionCompletionData.Builder b) {
        this.resultCode = b.resultCode;
        this.authorizationCode = b.authorizationCode;
        this.rrn = b.rrn;
        this.transactionNumber = b.transactionNumber;
        this.maskedCardNumber = b.maskedCardNumber;
        this.cardExpiryDate = b.cardExpiryDate;
        this.errorMessage = b.errorMessage;
        this.operationDateTime = b.operationDateTime;
        this.terminalNumber = b.terminalNumber;
        this.paymentSystemName = b.paymentSystemName;
        this.cardNumberHash = b.cardNumberHash;
        this.requestId = b.requestId;
        this.transactionTypeHostReader = b.transactionTypeHostReader;
        this.pinEntered = b.pinEntered;
        this.sign = b.sign;
        this.cardEntryType = b.cardEntryType;
        this.receipt = b.receipt;
    }
    
    public int getResultCode() {
        return this.resultCode;
    }
    
    public String getAuthorizationCode() {
        return this.authorizationCode;
    }
    
    public String getRrn() {
        return this.rrn;
    }
    
    public String getTransactionNumber() {
        return this.transactionNumber;
    }
    
    public String getMaskedCardNumber() {
        return this.maskedCardNumber;
    }
    
    public String getCardExpiryDate() {
        return this.cardExpiryDate;
    }
    
    public String getErrorMessage() {
        return this.errorMessage;
    }
    
    public Date getOperationDateTime() {
        return this.operationDateTime;
    }
    
    public String getTerminalNumber() {
        return this.terminalNumber;
    }
    
    public String getPaymentSystemName() {
        return this.paymentSystemName;
    }
    
    public String getCardNumberHash() {
        return this.cardNumberHash;
    }
    
    public int getRequestId() {
        return this.requestId;
    }

    public TransactionTypeHostReader getTransactionTypeHostReader() {
        return this.transactionTypeHostReader;
    }

    public boolean isPinEntered() {
        return pinEntered;
    }

    public boolean isSign() {
        return sign;
    }

    public CardTransType getCardEntryType() {
        return cardEntryType;
    }

    public String getReceipt() {
        return receipt;
    }

    @Override
    public String toString() {
        return "TransactionCompletionData{" +
                "resultCode=" + resultCode +
                ", authorizationCode='" + authorizationCode + '\'' +
                ", rrn='" + rrn + '\'' +
                ", transactionNumber='" + transactionNumber + '\'' +
                ", maskedCardNumber='" + maskedCardNumber + '\'' +
                ", cardExpiryDate='" + cardExpiryDate + '\'' +
                ", errorMessage='" + errorMessage + '\'' +
                ", operationDateTime=" + operationDateTime +
                ", terminalNumber='" + terminalNumber + '\'' +
                ", paymentSystemName='" + paymentSystemName + '\'' +
                ", cardNumberHash='" + cardNumberHash + '\'' +
                ", requestId=" + requestId +
                '}';
    }

    public static class Builder {
        private int resultCode;
        private String authorizationCode;
        private String rrn;
        private String transactionNumber;
        private String maskedCardNumber;
        private String cardExpiryDate;
        private String errorMessage;
        private Date operationDateTime;
        private String terminalNumber;
        private String paymentSystemName;
        private String cardNumberHash;
        private int requestId;
        private TransactionTypeHostReader transactionTypeHostReader;
        private CardTransType cardEntryType;
        private boolean pinEntered;
        private boolean sign;
        private String receipt;
        
        public Builder() {
        }
        
        public TransactionCompletionData.Builder resultCode(int resultCode) {
            this.resultCode = resultCode;
            return this;
        }
        
        public TransactionCompletionData.Builder authorizationCode(String authorizationCode) {
            this.authorizationCode = authorizationCode;
            return this;
        }
        
        public TransactionCompletionData.Builder rrn(String rrn) {
            this.rrn = rrn;
            return this;
        }
        
        public TransactionCompletionData.Builder transactionNumber(String transactionNumber) {
            this.transactionNumber = transactionNumber;
            return this;
        }
        
        public TransactionCompletionData.Builder maskedCardNumber(String maskedCardNumber) {
            this.maskedCardNumber = maskedCardNumber;
            return this;
        }
        
        public TransactionCompletionData.Builder cardExpiryDate(String cardExpiryDate) {
            this.cardExpiryDate = cardExpiryDate;
            return this;
        }
        
        public TransactionCompletionData.Builder errorMessage(String errorMessage) {
            this.errorMessage = errorMessage;
            return this;
        }
        
        public TransactionCompletionData.Builder operationDateTime(Date operationDateTime) {
            this.operationDateTime = operationDateTime;
            return this;
        }
        
        public TransactionCompletionData.Builder terminalNumber(String terminalNumber) {
            this.terminalNumber = terminalNumber;
            return this;
        }
        
        public TransactionCompletionData.Builder paymentSystemName(String paymentSystemName) {
            this.paymentSystemName = paymentSystemName;
            return this;
        }
        
        public TransactionCompletionData.Builder cardNumberHash(String cardNumberHash) {
            this.cardNumberHash = cardNumberHash;
            return this;
        }
        
        public TransactionCompletionData.Builder requestId(int requestId) {
            this.requestId = requestId;
            return this;
        }

        public TransactionCompletionData.Builder transactionTypeD200(TransactionTypeHostReader transactionTypeHostReader) {
            this.transactionTypeHostReader = transactionTypeHostReader;
            return this;
        }

        public Builder pinEntered(boolean pinEntered) {
            this.pinEntered = pinEntered;
            return this;
        }

        public Builder sign(boolean sign) {
            this.sign = sign;
            return this;
        }

        public Builder cardEntryType(CardTransType cardEntryType) {
            this.cardEntryType = cardEntryType;
            return this;
        }

        public Builder receipt(String receipt) {
            this.receipt = receipt;
            return this;
        }

        public TransactionCompletionData build() {
            return new TransactionCompletionData(this);
        }
    }
}
