package ru.m4bank.cardreaderlib.readers.host.icmp;

import m4bank.ru.icmplibrary.operation.enums.CardEntryType;
import ru.m4bank.cardreaderlib.enums.CardTransType;

/**
 * Converter from common card transaction types to ICMP card transaction types.
 *
 */

public class IcmpCardTransTypeConverter {
    
    /**
     * Convert {@link CardEntryType} into {@link CardTransType}.
     *
     * @param in The common card transaction type
     *
     * @return Return converted into ICMP card transaction type
     */
    
    public static CardTransType convert(CardEntryType in) {
        if(in == null)
            return null;
        switch (in) {
            case MAGNETIC_STRIPE:
            case FALLBACK:
                return CardTransType.MAGNETIC_STRIPE;
            case CHIP:
                return CardTransType.CONTACT_EMV;
            case CONTACTLESS_MS:
            case CONTACTLESS_EMV:
                return CardTransType.CONTACTLESS_EMV;
            default:
                return CardTransType.UNKNOWN;
        }
    }
}
