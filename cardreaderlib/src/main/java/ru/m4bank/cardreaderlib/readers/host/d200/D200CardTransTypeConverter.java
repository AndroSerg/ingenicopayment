package ru.m4bank.cardreaderlib.readers.host.d200;

import ru.m4bank.cardreaderlib.enums.CardTransType;
import ru.m4bank.util.d200lib.enums.CardEntryType;

/**
 * Converter from common card transaction types to D200 card transaction types.
 *
 */

public class D200CardTransTypeConverter {
    
    /**
     * Convert {@link CardEntryType} into {@link CardTransType}.
     *
     * @param in The common card transaction type
     *
     * @return Return converted into D200 card transaction type
     */
    
    public static CardTransType convert(CardEntryType in) {
        switch (in) {
            case MAGNETIC_STRIPE:
            case FALLBACK:
                return CardTransType.MAGNETIC_STRIPE;
            case CHIP:
                return CardTransType.CONTACT_EMV;
            case CONTACTLESS_MS:
            case CONTACTLESS_EMV:
                return CardTransType.CONTACTLESS_EMV;
            default:
                return CardTransType.UNKNOWN;
        }
    }
    
}
