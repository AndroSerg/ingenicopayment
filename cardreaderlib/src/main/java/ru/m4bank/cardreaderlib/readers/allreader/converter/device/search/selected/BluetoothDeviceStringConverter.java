package ru.m4bank.cardreaderlib.readers.allreader.converter.device.search.selected;

import android.bluetooth.BluetoothDevice;

import com.google.common.base.Converter;

/**
 * Common class for creating device name by bluetooth device.
 *
 */

public class BluetoothDeviceStringConverter extends Converter<BluetoothDevice, String> {
    
    /**
     * Create device name by bluetooth device.
     *
     * @param device bluetooth device
     *
     * @return Return generated device name
     *
     */
    
    @Override
    protected String doForward(BluetoothDevice device) {
        return device.getAddress() + "-" + device.getName();
    }
    
    /**
     * Create bluetooth device by device name.
     *
     * @param s device name
     *
     * @return Return bluetooth device
     *
     */

    @Override
    protected BluetoothDevice doBackward(String s) {
        return null;
    }
}
