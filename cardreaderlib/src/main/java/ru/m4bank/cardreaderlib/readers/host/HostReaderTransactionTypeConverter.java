package ru.m4bank.cardreaderlib.readers.host;

import ru.m4bank.cardreaderlib.enums.TransactionType;

/**
 * Common transaction type converter class.
 *
 */

public class HostReaderTransactionTypeConverter {
    
    /**
     * Convert {@link TransactionType} into {@link TransactionTypeHostReader}.
     *
     * @param transactionType The general type of transaction
     *
     * @return Return converted into common transaction type
     */
    
    public TransactionTypeHostReader convert(TransactionType transactionType) {
        TransactionTypeHostReader transactionTypeHostReader;
        switch (transactionType) {
            case PAYMENT:
                transactionTypeHostReader = TransactionTypeHostReader.PAYMENT;
                break;
            case CANCEL:
                transactionTypeHostReader = TransactionTypeHostReader.CANCEL;
                break;
            case REFUND:
                transactionTypeHostReader = TransactionTypeHostReader.REFUND;
                break;
            case RECONCILIATION:
                transactionTypeHostReader = TransactionTypeHostReader.RECONCILIATION;
                break;
            case REVERSAL_OF_LAST:
                transactionTypeHostReader = TransactionTypeHostReader.REVERSAL_OF_LAST;
                break;
            default:
                transactionTypeHostReader = TransactionTypeHostReader.UNKNOWN;
                break;
        }
        return transactionTypeHostReader;
    }
}
