package ru.m4bank.cardreaderlib.readers.allreader.converter.device.search.service;

import android.bluetooth.BluetoothDevice;

import java.util.List;

import ru.m4bank.cardreaderlib.readers.allreader.converter.SelectionService;
import ru.m4bank.cardreaderlib.readers.allreader.converter.device.search.selected.BluetoothDeviceStringConverter;

/**
 * Bluetooth device service for selection device class.
 *
 */

public class DeviceSelectionServiceBluetooth extends SelectionService<BluetoothDevice, List<BluetoothDevice>, BluetoothDeviceStringConverter> {

    public DeviceSelectionServiceBluetooth(List<BluetoothDevice> deviceList, BluetoothDeviceStringConverter converter) {
        super(deviceList, converter);
    }

}
