package ru.m4bank.cardreaderlib.readers.allreader.converter.components.transaction;

import ru.m4bank.cardreaderlib.data.TransactionComponents;
import ru.m4bank.util.d200lib.dto.TransactionInputData;
import ru.m4bank.util.d200lib.enums.OperationType;

/**
 * Converter transaction components for D200.
 *
 */

public class ConverterTransactionComponentsD200 extends ConverterTransactionComponents<TransactionInputData> {
    
    /**
     * Convert transaction components into {@link TransactionInputData} class.
     * @param transactionComponents transaction components
     */
    
    @Override
    public TransactionInputData createTransactionComponents(TransactionComponents transactionComponents) {
        long amount = Long.parseLong(transactionComponents.getAmount());
        String rrn = null;
        OperationType operationType;
        switch (transactionComponents.getTransactionType()) {
            case PAYMENT:
                operationType = OperationType.PAYMENT;
                break;
            case CANCEL:
                operationType = OperationType.REVERSE_LAST_PAYMENT;
                break;
            case REFUND:
                operationType = OperationType.REFUND;
                rrn = transactionComponents.getRrn();
                break;
            case RECONCILIATION:
                operationType = OperationType.RECONCILIATION;
                break;
            case ENTER_SERVICE_MENU:
                operationType = OperationType.ENTER_SERVICE_MENU;
                break;
            default:
                operationType = OperationType.RECONCILIATION;
                break;
        }
        return new TransactionInputData(amount, operationType, rrn);
    }
}
