package ru.m4bank.cardreaderlib.readers.host.d200;

import ru.m4bank.cardreaderlib.readers.host.TransactionCompletionData;
import ru.m4bank.cardreaderlib.readers.host.TransactionTypeHostReader;
import ru.m4bank.util.d200lib.dto.TransactionResultData;

/**
 * Converter from common transaction result data to D200 transaction result data.
 *
 */

public class D200TransactionCompletionDataConverter {
    
    /**
     * Convert {@link TransactionResultData} into {@link TransactionCompletionData}.
     *
     * @param in The common transaction result data
     *
     * @param transactionTypeHostReader The common transaction type
     *
     * @return Return converted into D200 transaction result data
     */
    
    public TransactionCompletionData convert(TransactionResultData in, TransactionTypeHostReader transactionTypeHostReader) {
        return new TransactionCompletionData.Builder()
                .authorizationCode(in.getAuthorizationCode())
                .cardExpiryDate(in.getCardExpiryDate())
                .cardNumberHash(in.getCardNumberHash())
                .errorMessage(in.getErrorMessage())
                .maskedCardNumber(in.getMaskedCardNumber())
                .operationDateTime(in.getOperationDateTime())
                .paymentSystemName(in.getPaymentSystemName())
                .requestId(in.getRequestId())
                .resultCode(in.getResultCode())
                .rrn(in.getRrn())
                .terminalNumber(in.getTerminalNumber())
                .transactionNumber(in.getTransactionNumber())
                .transactionTypeD200(transactionTypeHostReader)
                .pinEntered(in.isPinEntered())
                .sign(!in.isPinEntered())
                .cardEntryType(D200CardTransTypeConverter.convert(in.getCardEntryType()))
                .receipt(in.getReceipt())
                .build();
    }
}
