package ru.m4bank.cardreaderlib.readers.allreader.converter;

import com.google.common.base.Converter;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

/**
 * Common generic service for selection device class.
 *
 */

public class SelectionService<X, T extends List<X>, A extends Converter<X, String>> {
    protected final T deviceList;
    protected final A converter;
    
    public SelectionService(T deviceList, A converter) {
        this.deviceList = deviceList;
        this.converter = converter;
    }
    
    /**
     * Return list of device names.
     * @return Return list of device names.
     */
    
    public List<String> getSelectionList() {
        return newArrayList(converter.convertAll(deviceList));
    }
    
    /**
     * Select {@link X} class object by name.
     *
     * @param id device name
     *
     * @return Return {@link X} class object.
     */
    
    public X getObject(String id) {
        for (X device : deviceList) {
            if (id.equals(converter.convert(device))) {
                return device;
            }
        }
        return null;
    }
}

