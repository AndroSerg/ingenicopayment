package ru.m4bank.cardreaderlib.readers.allreader.converter.components.transaction;

import ru.m4bank.cardreaderlib.data.TransactionComponents;

/**
 * Converter transaction components.
 *
 */

public class ConverterTransactionComponents<T> {
    
    /**
     * Convert transaction components into T class.
     * @param transactionComponents transaction components
     */
    
    public T createTransactionComponents(TransactionComponents transactionComponents) {
        return null;
    }
}
