package ru.m4bank.cardreaderlib.readers.host.icmp;

import m4bank.ru.icmplibrary.dto.TransactionResultData;
import m4bank.ru.icmplibrary.operation.enums.CardPaymentSystem;
import ru.m4bank.cardreaderlib.readers.host.TransactionCompletionData;
import ru.m4bank.cardreaderlib.readers.host.TransactionTypeHostReader;

/**
 * Converter from common transaction result data to ICMP transaction result data.
 *
 */

public class IcmpTransactionCompletionDataConverter {
    
    /**
     * Convert {@link TransactionResultData} into {@link TransactionCompletionData}.
     *
     * @param in The common transaction result data
     *
     * @param transactionTypeHostReader The common transaction type
     *
     * @return Return converted into ICMP transaction result data
     */
    
    public TransactionCompletionData convert(TransactionResultData in, TransactionTypeHostReader transactionTypeHostReader) {
        CardPaymentSystem cardPaymentSystem = in.getPaymentSystemName();
        String paymentSystem = (cardPaymentSystem != null) ? cardPaymentSystem.name() : CardPaymentSystem.UNKNOWN.name();

        return new TransactionCompletionData.Builder()
                .authorizationCode(in.getAuthorizationCode())
                .cardExpiryDate(in.getCardExpiryDate())
                .cardNumberHash(in.getCardNumberHash())
                .errorMessage(in.getErrorMessage())
                .maskedCardNumber(in.getMaskedCardNumber())
                .operationDateTime(in.getOperationDateTime())
                .paymentSystemName(paymentSystem)
                .requestId(in.getRequestId())
                .resultCode(Integer.parseInt(in.getResultCode()))
                .rrn(in.getRrn())
                .terminalNumber(in.getTerminalNumber())
                .transactionNumber(in.getTransactionNumber())
                .transactionTypeD200(transactionTypeHostReader)
                .pinEntered(in.isPinEntered())
                .sign(in.isSignature())
                .cardEntryType(IcmpCardTransTypeConverter.convert(in.getCardEntryType()))
                .receipt(in.getReceipt())
                .build();
    }
}
