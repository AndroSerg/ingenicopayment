package ru.m4bank.cardreaderlib.data;

/**
 * Components for reversal.
 *
 */

public class ReversalTransactionComponents {

    private String merchantId;

    private String operationNumber;

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getOperationNumber() {
        return operationNumber;
    }

    public void setOperationNumber(String operationNumber) {
        this.operationNumber = operationNumber;
    }
}
