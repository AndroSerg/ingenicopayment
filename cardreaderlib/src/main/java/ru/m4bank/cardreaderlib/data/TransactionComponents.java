package ru.m4bank.cardreaderlib.data;

import java.io.Serializable;

import ru.m4bank.cardreaderlib.enums.TransactionType;

/**
 * Components for transaction.
 *
 */

public class TransactionComponents implements Serializable {

    private String currency;
    private String currencyExponent;
    private String countryCode;
    private String amount;
    private TransactionType transactionType;

    private String rrn;
    private String merchantId;

    public TransactionComponents() {
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCurrencyExponent() {
        return currencyExponent;
    }

    public void setCurrencyExponent(String currencyExponent) {
        this.currencyExponent = currencyExponent;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public TransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public String getRrn() {
        return rrn;
    }

    public void setRrn(String rrn) {
        this.rrn = rrn;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }
}


