package ru.m4bank.connectionreaders.activate.enums;

/**
 * Supported readers.
 *
 */

public enum ReaderType {
    UNKNOWN("0"), D200("12"), ICMP_UPOS("14");

    private final String code;

    ReaderType(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public static ReaderType getByCode(String code) {
        for (ReaderType readerType : ReaderType.values()) {
            if (readerType.getCode().equals(code)) return readerType;
        }
        return UNKNOWN;
    }
}
