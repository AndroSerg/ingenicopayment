package ru.m4bank.connectionreaders.activate;

import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

import java.util.List;

import ru.m4bank.cardreaderlib.manager.CardReader;
import ru.m4bank.cardreaderlib.manager.methods.output.CardReaderConnectionHandler;
import ru.m4bank.connectionreaders.activate.enums.ReaderType;
import ru.m4bank.connectionreaders.activate.handler.ConnectionReader;

/**
 *
 * Manager that provides working with card readers. It provides selecting card reader and
 * connection with it.
 *
 */

public class ManagerStateReader implements CardReaderConnectionHandler {

    private CardReader cardReader;
    private ReaderType readerType;

    private boolean connectedDevice;
    private boolean startNewReader;

    private Context context;
    private ConnectionReader connectionReader;

    public ManagerStateReader(Context context) {
        cardReader = null;
        connectedDevice = false;
        startNewReader = false;

        this.context = context;
    }

    public ManagerStateReader(Context context, ConnectionReader connectionReader) {
        this(context);
        setConnectionReader(connectionReader);
    }
    
    /**
     * Sets {@link ConnectionReader} callbacks to work with card reader
     *
     * @param connectionReader Specified {@link ConnectionReader} callbacks
     *
     */

    public void setConnectionReader(ConnectionReader connectionReader) {
        this.connectionReader = connectionReader;
    }
    
    /**
     * Connect if need with specified by name card reader and return connected device
     *
     * @param cardReader Specified card reader
     *
     * @param deviceName Specified the name of the card reader to select from the available devices
     *
     * @return Return connected card reader
     */

    public CardReader activateReader(CardReader cardReader, String deviceName) {
        this.cardReader = cardReader;
        if (!cardReader.isConnected()) {
            cardReader.selectedReader(deviceName);
        }
        return cardReader;
    }
    
    /**
     * If there are no already connected devices, create new one with specified type and connect
     * to it. Otherwise, we disconnect from the old device, create a new one and connect to it.
     *
     * @param readerType Specified the card reader type
     *
     * @return Return connected card reader
     */
    
    public CardReader activateReader(ReaderType readerType) {

        this.readerType = readerType;

        if(cardReader == null || !connectedDevice) {
            startNewReader = false;
            initCardReader(readerType, context);
            cardReader.connect();
        } else {
            startNewReader = true;
            cardReader.disconnect();
            connectedDevice = false;
            initCardReader(readerType, context);
        }
        return cardReader;
    }
    
    /**
     * Connect with specified by name card reader device
     *
     * @param name Specified the name of the device
     *
     */

    public void selectReaderFromList(String name) {
        cardReader.selectedReader(name);
    }
    
    /**
     * If there is some connected device disconnect from it.
     *
     */
    
    public void stopReader() {
        startNewReader = false;
        if(cardReader != null)
            cardReader.disconnect();
    }
    
    /**
     * Check for connected devices.
     *
     * @return Return true if there is some connected device otherwise return false.
     */
    
    public boolean getConnectedDevice() {
        return connectedDevice;
    }
    
    /**
     * Initiate card reater from specified type
     *
     * @param readerType Specified the type of the device
     *
     * @param context context
     */
    
    private void initCardReader (ReaderType readerType, Context context) {
        SelectCardReaderManager selectCardReaderManager = new SelectCardReaderManager(readerType);
        cardReader = selectCardReaderManager.create(context, this);
    }
    
    /**
     * Connect with new device or call disconnected callback from old.
     *
     */
    
    private void launchNewReader() {
        if(startNewReader)
            cardReader.connect();
        else {
            cardReader = null;
            connectionReader.onDisconnected(cardReader);
        }
    }
    
    /**
     * Called when reader has connected. Register receiver and call corresponded external callback.
     *
     */
    
    @Override
    public void onReaderConnected() {
        connectedDevice = true;

        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        context.registerReceiver(mBluetoothReceiver, filter);

        connectionReader.onConnected(cardReader);
    }
    
    /**
     * Called when reader has disconnected. Unregister receiver and call function with callbacks.
     *
     */
    
    @Override
    public void onReaderDisconnected() {
        connectedDevice = false;

        unregisterReceiver();

        launchNewReader();
    }
    
    /**
     * Called when reader has disconnected. Unregister receiver.
     *
     */
    
    @Override
    public void onReaderDisconnectedWithOutCallbacks() {
        unregisterReceiver();

        connectedDevice = false;
    }
    
    /**
     * Called when error with reader has occured. Call corresponded external callback.
     *
     */
    
    @Override
    public void onReaderError() {
        connectedDevice = false;
        connectionReader.onConnectedError(cardReader);
    }
    
    /**
     * Called when device list is available. Call corresponded external callback.
     *
     * @param listDevice Specified the type of the device
     *
     */
    
    @Override
    public void viewDeviceOfList(List<String> listDevice) {
        connectionReader.onDeviceList(listDevice);
    }
    
    /**
     * Return saved card reader
     *
     * @return Return saved card reader
     *
     */
    
    public CardReader getCardReader() {
        return cardReader;
    }
    
    /**
     * Specified card reader
     *
     * @param cardReader saved card reader
     *
     */
    
    public void setCardReader(CardReader cardReader) {
        this.cardReader = cardReader;
    }
    
    

    private BroadcastReceiver mBluetoothReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
            Log.d("123", "Device " + device.getName() + ", received " + action + " event.");
            if (action.equals(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED) || action.equals(BluetoothDevice.ACTION_ACL_DISCONNECTED)) {
                stopReader();
            }
        }
    };
    
    /**
     * Unregister bluetooth receiver
     *
     */
    
    private void unregisterReceiver() {
        try {
            context.unregisterReceiver(mBluetoothReceiver);
        } catch (Exception ignored) {

        }
    }
}
