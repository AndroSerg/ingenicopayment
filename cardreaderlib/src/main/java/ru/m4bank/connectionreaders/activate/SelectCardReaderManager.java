package ru.m4bank.connectionreaders.activate;

import android.content.Context;

import ru.m4bank.cardreaderlib.manager.CardReader;
import ru.m4bank.cardreaderlib.manager.CardReaderD200;
import ru.m4bank.cardreaderlib.manager.CardReaderIcmp;
import ru.m4bank.cardreaderlib.manager.methods.output.CardReaderConnectionHandler;
import ru.m4bank.connectionreaders.activate.enums.ReaderType;

/**
 * Manager for =creating card readers of the specified type
 *
 */

public class SelectCardReaderManager {

    private ReaderType readerType;

    public SelectCardReaderManager (ReaderType readerType) {
        this.readerType = readerType;
    }
    
    /**
     * Create card reader specified in the constructor type.
     *
     * @param context The context.
     * @param cardReaderConnectionHandler The handler with the required callbacks

     * @return Return facade card reader specified in the constructor type .
     */
    
    public CardReader create(Context context, CardReaderConnectionHandler cardReaderConnectionHandler) {

        CardReader cardReader;

        switch (readerType) {
            case D200:
                cardReader = CardReaderD200.getNewInstance(context, cardReaderConnectionHandler);
                break;
            case ICMP_UPOS:
                cardReader = CardReaderIcmp.getNewInstance(context, cardReaderConnectionHandler);
                break;
            default:
                cardReader = null;
                break;
        }
        return cardReader;
    }
}
