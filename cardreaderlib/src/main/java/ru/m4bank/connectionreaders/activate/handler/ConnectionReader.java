package ru.m4bank.connectionreaders.activate.handler;

import java.util.List;

import ru.m4bank.cardreaderlib.manager.CardReader;

/**
 *
 * Callback interface for providing card reader connection process
 *
 */

public interface ConnectionReader {
    
    /**
     * Called when card reader has connected
     *
     * @param cardReader Specified card reader
     *
     */

    public void onConnected(CardReader cardReader);
    
    /**
     * Called when card reader has disconnected
     *
     * @param cardReader Specified card reader
     *
     */

    public void onDisconnected(CardReader cardReader);
    
    /**
     * Called when connection error has occurred
     *
     * @param cardReader Specified card reader
     *
     */

    public void onConnectedError(CardReader cardReader);
    
    /**
     * Called when device list has available
     *
     * @param list Available device list
     *
     */

    public void onDeviceList(List<String> list);
    
}
